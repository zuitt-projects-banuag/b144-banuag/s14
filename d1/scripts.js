// Functions

function printStar() {
	console.log("*");
	console.log("**");
	console.log("***");
}

/*Example of one parameters
function sayHello(name){
	console.log('Hello ' + name);
}

sayHello(6)*/

/*Mag aalert ng hello then magprint ng hello sa console
function alertPrint() {
	alert("hello")
	console.log("alertPrint")
}

alertPrint()

function alertPrint() {
	alert("Hello");
	A function can call a function outside.
	sayHello();
}
*/

// Funtion that accepts two numbers and print the sum
// Parameter is the x and y
/*function addSum(x,y){
	let sum = x + y;
	console.log(sum);
}*/

/*addSum(13,2); arguments
addSum(23,56);*/

// Function with 3 parameters
// String Template Literals
// Interpolation ${sample}
function printBio(lname,fname,age) {
	// console.log('Hello ' + lname +''+ fname +''+ age)
	// 
	console.log(`Hello Ms.${lname}${fname}${age}`)
}

// Return keyword
function addSum(x,y){
	return y-x //return is return the value
	//console.log(x+y); console is print something in console
}
let sumNum = addSum(3,4)

