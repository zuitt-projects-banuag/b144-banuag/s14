/*Writing Comment in JavaScript:*/
// one-line comments 
/* multi-line comment - ctrl + shift + / */

// Statment
console.log("Hello Batch 144!")

/*
	JavaScript - we can see or log message in our console.

	Browser Console are part of our browser which will allow us to see/log messages, data or information from our programming language - JavaScript.

	For most browsers, console are easily accessed through its developer tools in the console tab.

	In fact, consoles is browser will allow us to add some JavaScript expressions.

	Statements 

	Statements are instructions, expressions that we add to our programming language to communicate with our computers.

	JS Statements usually ends in a semi colon (;), However, JS has implemented a way to automatically add semicolons at the end of a line/statement. Semi Colons can actually be omitted in creating JS statements but Semicolons in JS are added to mark the end of the statement.

	Syntax

	Syntax in programming is a set of rules that describes how statements are properly made made/constructed.

	Lines/blocks of code must follow a certain of rules for it to work properly.

*/

	console.log("Rocel Banuag");

	/*Mini Activity 
		Create three console logs to display the following message:

		console.log("<yourFavoriteFood>")
	*/
		/*console.log("Chicken Spaghetti");
		console.log("Chicken Spaghetti");
		console.log("Chicken Spaghetti");*/
	
	let food1 = "Chicken Spaghetti";
	console.log(food1);
	/*
		Variable are a way to store information or data within our JS.

		To create a variable we first declare the name of the variable with either the let/const keyword:

		let nameOfVariable

		Then, we can initialize the variable with a value or data.

		let nameOfVariable = <data>;
	*/

	console.log("My favorite food is: " + food1);

	let	food2 = "Pizza";

	console.log("My favorite food are: " + food1 + " and " + food2);
	let food3;
	/*
		We can create variables without add an initial value, however, the variable's content is undefined.
	*/
	console.log(food3);

	food3 = "Chickenjoy";
	/*
		We can update the content of a let variable by reassigning the content using an assignment operator (=);

		Assignment operator (=) = lets us assign data to a variable.
	*/
	console.log(food3);

	/*Mini Activity
		Reassign variable food1 with new data, with another of your favorite food.

		Reassign variable food2 with new data, with another of your favorite food.

		Log the values of both variables in the console.
	*/

	food1 = "Palabok";
	food2 = "Samgyup";

	console.log(food1);
	console.log(food2);

	/*
		We can update our variables with an assignment operator without needing to use the let keyword again.

		We cannot create another variable with the same name. It will result in an error.
	*/

	// Const Keyword

	/*
		Const keyword will also allow us to create variables. However, with a const keyword we create constant variables, which means these data to saved in a constant will not change, cannot be changed and should not be changed.
	*/

	const pi = 3.1416;
	console.log(pi);
	// Trying to re-assign a const variable will result into an error.
	/*pi = "Pizza";
	console.log(pi)*/

	/*const gravity;
	console.log(gravity);*/

	//We also cannot declare/create a const variable without initialization or an initial value.

	/*Mini Activity*/
	let	myName;
	const sunriseDirection = "East";
	const sunsetDirection = "West";
	/*You can actually log multiple items, variables, data in console,log separated by , */
	console.log(myName, sunriseDirection, sunsetDirection);

	/*
		Guides in creating a JS variable:
		1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

		2. Creating a variable has two parts: Declaration of the variable name and Initialization of the initial value of the variable using assignment operator (=)

		3. A let variable can be declared without initialization. However, the value of the variable will be undefined until it is re-assigned with a value.

		4. Not defined vs Undefined. Not defined error means the variable is used but NOT declared. undefined results from a variable that is used but not initialized. 

		5. We can use const keyword to create constant variables. Constant variables cannot be declared without initialization.
		Constant variable cannot be re-assigned.

		6. When creating variable names, start with small caps, this is because of avoding conflict with syntax in JS that is named with capital letters.

		7. If the variable would need two words, the naming convection is the use of camelCase. Do not add a space variable with words as names.	
	*/

	/*Data Types
	   Strings are data which are alphanumerical text. It could be a name, a phrase or even a sentence. We can create string with single quote ('') or with double quote ("").

	   variable = "string data"

	*/

	console.log("Sample String Data");

	let country = "Philippines";
	let province = "Rizal";
	console.log(country, province);

	/*Concatenation - is a way for us to add strings together and have a single string. We can use our "+" symbol for this.*/

	let fullAddress = province + ',' + country
	console.log(fullAddress);

	let	greeting = "I live in " + country;
	console.log(greeting);

	/*In JS, when you use the + sign with string we have concatenation.*/

	/*Numericals String*/
	let	numString = "50";
	let numString2 = "25";
	console.log(numString + numString2)

	/*Strings have property called .length and it tells us the number characters in a string.

	Spaces, commas, periods can also be character when included in string.

	It will return a number type data.
	*/

	let hero = "Captain America";
	console.log(hero.length);

	/*Number Type
		Number Type data can actually be used in proper mathematical equations and operations.
	*/
	let students = 16;
	console.log(students)

	let num1 = 50;
	let num2 = 25;
	/*Additon operator will allow us to add two number types and return the sum as a number data type.

	Operations return a value and that value can be saved in a variable.

	What if we use the addition operators on a number type and a numerical string?
		- it results to concatenation.

	parseInt() will allow us to turn a numeric string into a proper number type.
	*/
	console.log(num1 + num2);

	let sum1 = num1 + num2;

	console.log(sum1);
	let numString3 = "100";
	let sum2 = parseInt(numString3) + num1
	console.log(sum2);

	const name = "Jeff";
	console.log(num1,numString3);

	/*Mini Activity*/
	let sum3 = sum1 + sum2;
	let sum4 = parseInt(numString2) + num2;

	console.log(sum3,sum4)

	/*Subtraction Operator 
		will let us get the difference between two number types. It will also result to a proper number type of data.

		When subtraction operator is used on a string and a number, the string will be converted into a number automatically and then JS will perform the operation.

		This automatic conversion from one type to another is called type converstion or type coercion or forced coercion.

		When a text string is subtracted with a number, it will result in NaN or Not a number when JS converted the string it result to NaN and NaN-Number = NaN
	*/
	let difference = num1 - num2;
	console.log(difference);

	let difference2 = numString3 - num2;
	console.log(difference2);

	let string1 = "fifteen";
	console.log(num2 - string1);

	let difference3 = hero - num2;
	console.log(difference3);

	/*Multiplication Operator (*)*/
	let num3 = 10;
	let num4 = 5;
	let product = num3*num4;
	console.log(num3*num4);
	let product2 = numString3 * num3;
	console.log(numString3 + num3);
	let product3 = numString * numString3;
	console.log(numString * numString3);

	/*Division Operator*/
	let num5 = 30;
	let num6 = 3;
	let quotient = num5/num6;
	console.log(quotient);
	let quotient2 = numString3/5;
	console.log(numString3/5);
	let quotient3 = 450/num4;
	console.log(quotient3);

	/*Boolean (true or false)
		Boolean is usually used for logical operation or for if-else conditions.

		Naming convertion for a variable that contains boolean is a yes or no question.
	*/

	let isAdmin = true;
	let isMarried = false;

	/* You can actually name your variable any way you want however as the best practice it should be:
		1. appropriate.
		2. definitive of the value it contains.
		3. semantically correct.
		
	*/

	/*Arrays
		arrays are a special kind of data type wherein we can store multiple values. 

		An array can store multiple values and even of different types. For best pracrice, keep the data type of items in an array uniforms.

		Values in an array are separated by comma. Failing to do so, will result in an error.

		An array is created with an Array Literal ([]) 

	*/
	let koponanNiEugene = ["Eugene", "Alfred", "Dennis", "Vincent"];
	console.log(koponanNiEugene);

	/*Array indices are markers of the order of the items in the array. Array index starts at 0*/

	// To acces an array item: arrayName[]
	console.log(koponanNiEugene[0]);
	// Bad Practice for an Array:
	let array2 = ["One Punch Man",true,500,"Saitama"]
	console.log(array2);

	// Objects
	/*
		Object are another kind of special data type used to mimic real world objects.
		With this we can add information that make sense, thematically relevant, and of different data types.

		Objects can be created with Object Literal ({})

		Each value is given a label which makes the data significant and meaningful this pairing of data and "Label" is what we call a Key-Value pair.

		a key-value pair together is called a property.

		Each property is separated by a comma.
	*/

	let person1 = {
		heroName: "One Punch Man", 
		isRegisted: true,
		salary: 500,
		realName: "Saitama"
	};
	// To get the value of an object's property, we can access it using dot notation.
	// objectName.propertyName
	console.log(person1.realName);

	/*Mini Activity*/
	let myFavoriteBands = ["Lany", "Weslife", "December Avenue","The Juans", "I Belong to the Zoo" ]
	console.log(myFavoriteBands);

	let me = {
		firstName: "Rocel", 
		lastName: "Banuag",
		isWebDeveloper: true,
		hasPortfolio: true,
		age: 24,
		/*Properties of an object should relate to each other and thematically relevant to describe a single item/project*/
		brand: "Toyota"
	};
	console.log(me);

	/*Undefined vs Null
		
		Null 
			- us the explicit declaration that there is no value.

		Undefined 
			- means that the variable exists however a value was not initialized with the variable.

	*/

	// Null
	let sampleNull = null;

	// Undefined
	let undefinedSample;
	console.log(undefinedSample);

	/*Certain process in programming explicity returns null to indicate that the task resulted to nothing.
	*/

	let foundResult = null;

	// For undefined, this is normally caused by developers creating variables that have no value or data associated with them.

	// The variable does exist but its value is still unknown.

	let person2 = {
		name: "Patricia",
		age: 28
	}
	// because the variable person2 does exist however the property isAdmin does not 
	console.log(person2.isAdmin);